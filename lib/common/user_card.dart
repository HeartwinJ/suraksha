import 'package:flutter/material.dart';
import 'package:suraksha/common/profile_page.dart';
import 'package:suraksha/common/utils.dart';
import 'package:suraksha/models/user_model.dart';

class UserCard extends StatelessWidget {
  final UserModel userData;
  const UserCard({Key? key, required this.userData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Card(
        color: Utils.getCardColor(userData),
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    userData.name,
                    style: const TextStyle(
                      fontSize: 24.0,
                    ),
                  ),
                  const SizedBox(
                    height: 8,
                  ),
                  Text(userData.phoneNum),
                ],
              ),
              Text(userData.profileStatus),
            ],
          ),
        ),
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ProfilePage(
              userData: userData,
            ),
          ),
        );
      },
    );
  }
}
