import 'package:flutter/material.dart';
import 'package:suraksha/common/utils.dart';
import 'package:suraksha/models/user_model.dart';
import 'package:suraksha/services/store_service.dart';

class ProfilePage extends StatelessWidget {
  final UserModel userData;
  const ProfilePage({Key? key, required this.userData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Profile'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Expanded(
              child: Card(
                color: Utils.getCardColor(userData),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        userData.name,
                        style: const TextStyle(
                          fontSize: 24.0,
                        ),
                      ),
                      const SizedBox(
                        height: 6,
                      ),
                      Text(userData.email),
                      const SizedBox(
                        height: 6,
                      ),
                      Text(userData.phoneNum),
                      const SizedBox(
                        height: 6,
                      ),
                      Text(userData.profileStatus),
                    ],
                  ),
                ),
              ),
            ),
            Column(
              children: [
                ['Negative', 'Primary'].contains(userData.profileStatus)
                    ? ElevatedButton(
                        child: const Text('Change to Positive'),
                        onPressed: () {
                          StoreService _store = StoreService();
                          _store.updateProfileStatus(userData.uid!, 'Positive');
                          Navigator.popUntil(context, (route) => route.isFirst);
                        },
                      )
                    : Container(),
                userData.profileStatus == 'Negative'
                    ? ElevatedButton(
                        child: const Text('Change to Primary'),
                        onPressed: () {
                          StoreService _store = StoreService();
                          _store.updateProfileStatus(userData.uid!, 'Primary');
                          Navigator.popUntil(context, (route) => route.isFirst);
                        },
                      )
                    : Container(),
                ['Primary', 'Positive'].contains(userData.profileStatus)
                    ? ElevatedButton(
                        child: const Text('Change to Negative'),
                        onPressed: () {
                          StoreService _store = StoreService();
                          _store.updateProfileStatus(userData.uid!, 'Negative');
                          Navigator.popUntil(context, (route) => route.isFirst);
                        },
                      )
                    : Container(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
