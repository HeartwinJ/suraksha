import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:suraksha/pages/admin/admin_dashboard.dart';
import 'package:suraksha/pages/home_page.dart';
import 'package:suraksha/pages/user/user_dashboard.dart';
import 'package:suraksha/services/auth_service.dart';

class AuthWrapper extends StatelessWidget {
  const AuthWrapper({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<User?>(context);
    final AuthService _auth = AuthService();

    if (user != null) {
      return FutureBuilder(
        future: _auth.getUserType(user),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return snapshot.data == 'ADMIN' ? const AdminDashboard() : const UserDashboard();
          }
          return const Scaffold(
            body: Center(
              child: Icon(
                Icons.masks,
                size: 69 + 69,
              ),
            ),
          );
        },
      );
    }
    return const HomePage();
  }
}
