import 'package:flutter/material.dart';
import 'package:suraksha/services/store_service.dart';

class DashboardComponent extends StatefulWidget {
  const DashboardComponent({Key? key}) : super(key: key);

  @override
  State<DashboardComponent> createState() => _DashboardComponentState();
}

class _DashboardComponentState extends State<DashboardComponent> {
  int _totalUsers = 0;
  int _totalPositiveUsers = 0;

  @override
  void initState() {
    super.initState();
    StoreService _store = StoreService();
    _store.getUsersList().then(
          (value) => setState(() {
            _totalUsers = value.size;
          }),
        );
    _store.getPositiveUsersList().then(
          (value) => setState(() {
            _totalPositiveUsers = value.size;
          }),
        );
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Card(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  const Text(
                    'Total Users',
                    style: TextStyle(
                      fontSize: 18.0,
                    ),
                  ),
                  Text(
                    _totalUsers.toString(),
                    style: const TextStyle(
                      fontSize: 48.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Card(
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  const Text(
                    'Total Cases',
                    style: TextStyle(
                      fontSize: 18.0,
                    ),
                  ),
                  Text(
                    _totalPositiveUsers.toString(),
                    style: const TextStyle(
                      fontSize: 48.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
