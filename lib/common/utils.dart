import 'package:flutter/material.dart';
import 'package:suraksha/models/user_model.dart';

class Utils {
  static Color? getCardColor(UserModel userData) {
    if (userData.profileStatus == 'Primary') {
      return Colors.yellow[700];
    } else if (userData.profileStatus == 'Positive') {
      return Colors.red[300];
    } else {
      return Colors.green;
    }
  }
}
