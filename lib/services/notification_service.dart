import 'package:flutter_local_notifications/flutter_local_notifications.dart';

FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();
const AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/ic_launcher');
const InitializationSettings initializationSettings = InitializationSettings(android: initializationSettingsAndroid);

void selectNotification(String? payload) {
  print('Notification Clicked!');
}

void initializeNotifications() async {
  await flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: selectNotification);
}

void sendNotification(String title, String body) async {
  AndroidNotificationDetails androidPlatformChannelSpecifics = AndroidNotificationDetails(
    'suraksha',
    'Suraksha Notification Channel',
    channelDescription: 'Suraksha Notification Channel',
    importance: Importance.max,
    priority: Priority.high,
    icon: '@mipmap/ic_launcher',
    styleInformation: BigTextStyleInformation(body),
  );
  NotificationDetails platformChannelSpecifics = NotificationDetails(android: androidPlatformChannelSpecifics);
  await flutterLocalNotificationsPlugin.show(69, title, body, platformChannelSpecifics);
}
