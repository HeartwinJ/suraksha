import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geolocator/geolocator.dart';
import 'package:suraksha/models/user_model.dart';

class StoreService {
  final CollectionReference _users = FirebaseFirestore.instance.collection('users');

  Future<DocumentSnapshot?> getUserDocument(String uid) async {
    DocumentSnapshot? doc;
    try {
      await _users.doc(uid).get().then((result) => doc = result);
    } catch (error) {
      print(error.toString());
    }
    return doc;
  }

  void createUser(uid, UserModel user, userType) async {
    try {
      _users.doc(uid).set({
        'name': user.name,
        'email': user.email,
        'designation': user.designation,
        'department': user.department,
        'gender': user.gender,
        'bloodGroup': user.bloodGroup,
        'phoneNum': user.phoneNum,
        'profileStatus': user.profileStatus,
        'userType': userType,
      });
    } catch (error) {
      print(error.toString());
    }
  }

  getUsersList() async {
    try {
      return _users.where('userType', isEqualTo: 'USER').get();
    } catch (error) {
      print(error);
    }
  }

  updateProfileStatus(String uid, String newProfileStatus) async {
    try {
      _users.doc(uid).update({'profileStatus': newProfileStatus});
    } catch (error) {
      print(error);
    }
  }

  updateLocation(String uid, Position curPos) {
    try {
      _users.doc(uid).update({'latitude': curPos.latitude, 'longitude': curPos.longitude});
    } catch (error) {
      print(error);
    }
  }

  getPositiveUsersList() async {
    List<UserModel> _usersList = [];
    try {
      return _users.where('userType', isEqualTo: 'USER').where('profileStatus', whereIn: ['Positive', 'Primary']).get();
    } catch (error) {
      print(error);
    }
  }
}
