import 'dart:math';

import 'package:geolocator/geolocator.dart';
import 'package:suraksha/models/user_model.dart';
import 'package:suraksha/services/auth_service.dart';
import 'package:suraksha/services/notification_service.dart';
import 'package:suraksha/services/store_service.dart';

double distance(lat1, lon1, lat2, lon2) {
  var p = 0.017453292519943295;
  var a = 0.5 - cos((lat2 - lat1) * p) / 2 + cos(lat1 * p) * cos(lat2 * p) * (1 - cos((lon2 - lon1) * p)) / 2;

  return 12742 * asin(sqrt(a));
}

void checkProximity() async {
  AuthService _auth = AuthService();
  StoreService _store = StoreService();
  LocationPermission permission;

  permission = await Geolocator.checkPermission();
  if (permission == LocationPermission.denied) {
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      return Future.error('Location permissions are denied');
    }
  }

  if (permission == LocationPermission.deniedForever) {
    return Future.error('Location permissions are permanently denied, we cannot request permissions.');
  }

  Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
  _store.updateLocation(_auth.curUid, position);

  List<UserModel> _positiveUsers = [];
  await _store.getPositiveUsersList().then((querySnapshot) {
    querySnapshot.docs.forEach((doc) {
      _positiveUsers.add(UserModel(
        uid: doc.id,
        name: doc['name'],
        email: doc['email'],
        designation: doc['designation'],
        department: doc['department'],
        gender: doc['gender'],
        bloodGroup: doc['bloodGroup'],
        phoneNum: doc['phoneNum'],
        profileStatus: doc['profileStatus'],
        latitude: doc['latitude'],
        longitude: doc['longitude'],
      ));
    });
  });

  if (_positiveUsers.any((user) => distance(position.latitude, position.longitude, user.latitude, user.longitude) < 0.01)) {
    sendNotification('You are in Contact!',
        'You have come in contact with someone who has tested positive or is a primary contact! Please follow all the necessary protocols and visit the Medical Center as soon as possible.');
  }
}
