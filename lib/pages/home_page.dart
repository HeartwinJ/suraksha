import 'package:flutter/material.dart';
import 'package:suraksha/pages/user_registration.dart';
import 'package:suraksha/services/auth_service.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              Expanded(
                child: GestureDetector(
                  child: const Icon(
                    Icons.masks,
                    size: 69 + 69,
                  ),
                  onLongPress: () {
                    AuthService _auth = AuthService();
                    _auth.signInWithGoogle();
                  },
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => const UserRegistration()));
                  },
                  child: const Text('Get Started'),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
