import 'package:flutter/material.dart';
import 'package:suraksha/models/nav_detail.dart';
import 'package:suraksha/common/dashboard.dart';
import 'package:suraksha/pages/admin/components/updates.dart';
import 'package:suraksha/pages/admin/search_users.dart';
import 'package:suraksha/services/auth_service.dart';

class AdminDashboard extends StatefulWidget {
  const AdminDashboard({Key? key}) : super(key: key);

  @override
  State<AdminDashboard> createState() => _AdminDashboardState();
}

class _AdminDashboardState extends State<AdminDashboard> {
  int _currentindex = 0;
  final List<NavDetail> _children = <NavDetail>[
    NavDetail(
      page: const DashboardComponent(),
    ),
    NavDetail(
      page: const UpdatesComponent(),
    ),
  ];

  _onNavTabTapped(int index) {
    setState(() {
      _currentindex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Admin Dashboard'),
        actions: [
          IconButton(
            onPressed: () {
              AuthService _auth = AuthService();
              _auth.signOut();
            },
            icon: const Icon(Icons.logout),
          ),
        ],
      ),
      body: _children.elementAt(_currentindex).page,
      floatingActionButton: FloatingActionButton.extended(
        isExtended: true,
        icon: const Icon(Icons.search),
        label: const Text('Search Users'),
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: ((context) => SearchUsersPage())));
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        onTap: _onNavTabTapped,
        currentIndex: _currentindex,
        items: const [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Dashboard',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.people),
            label: 'Updates',
          ),
        ],
      ),
    );
  }
}
