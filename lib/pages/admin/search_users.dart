import 'package:flutter/material.dart';
import 'package:suraksha/common/user_card.dart';
import 'package:suraksha/models/user_model.dart';
import 'package:suraksha/services/store_service.dart';

class SearchUsersPage extends StatefulWidget {
  SearchUsersPage({Key? key}) : super(key: key);

  @override
  State<SearchUsersPage> createState() => _SearchUsersPageState();
}

class _SearchUsersPageState extends State<SearchUsersPage> {
  List<UserModel> users = <UserModel>[];

  @override
  void initState() {
    super.initState();

    final StoreService _store = StoreService();
    _store.getUsersList().then((querySnapshot) {
      List<UserModel> _temp = [];
      querySnapshot.docs.forEach((doc) {
        _temp.add(UserModel(
          uid: doc.id,
          name: doc['name'],
          email: doc['email'],
          designation: doc['designation'],
          department: doc['department'],
          gender: doc['gender'],
          bloodGroup: doc['bloodGroup'],
          phoneNum: doc['phoneNum'],
          profileStatus: doc['profileStatus'],
          latitude: doc['latitude'],
          longitude: doc['longitude'],
        ));
      });
      setState(() {
        users = _temp;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Search Users'),
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(
          vertical: 8.0,
          horizontal: 4.0,
        ),
        child: ListView.builder(
          physics: const BouncingScrollPhysics(),
          itemCount: users.length,
          itemBuilder: (_, int index) => UserCard(userData: users.elementAt(index)),
        ),
      ),
    );
  }
}
