import 'package:flutter/material.dart';
import 'package:suraksha/services/auth_service.dart';

class UserRegistration extends StatefulWidget {
  const UserRegistration({Key? key}) : super(key: key);

  @override
  State<UserRegistration> createState() => _UserRegistrationState();
}

class _UserRegistrationState extends State<UserRegistration> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  // String _rollNum = '';
  String _designation = 'Student';
  String _department = 'CSE';
  String _gender = 'male';
  String _bloodGroup = 'A+';
  String _phoneNum = '';

  void _validateForm() async {
    AuthService _authService = AuthService();
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      await _authService.signUpWithGoogle(_designation, _department, _gender, _bloodGroup, _phoneNum).then((user) => {
            if (user != null) {Navigator.popUntil(context, (route) => route.isFirst)} else {print('An Error Occured!')}
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('User Registration'),
      ),
      body: SafeArea(
        child: Center(
          child: Form(
            key: _formKey,
            autovalidateMode: AutovalidateMode.disabled,
            child: Column(
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      vertical: 8.0,
                      horizontal: 16.0,
                    ),
                    child: ListView(
                      children: [
                        // TextFormField(
                        //   decoration: const InputDecoration(
                        //     border: OutlineInputBorder(),
                        //     labelText: 'Roll Number',
                        //   ),
                        //   onSaved: (input) {
                        //     _rollNum = input!;
                        //   },
                        //   validator: (input) => (input!.isEmpty)
                        //       ? 'Roll Number is Required'
                        //       : null,
                        // ),
                        DropdownButton<String>(
                          value: _designation,
                          onChanged: (String? newValue) {
                            setState(() {
                              _designation = newValue!;
                            });
                          },
                          items: <String>['Student', 'Teaching Staff', 'Non-Teaching Staff'].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                        DropdownButton<String>(
                          value: _department,
                          onChanged: (String? newValue) {
                            setState(() {
                              _department = newValue!;
                            });
                          },
                          items: <String>['CSE', 'ECE', 'EEE', 'ME', 'CE'].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                        DropdownButton<String>(
                          value: _gender,
                          onChanged: (String? newValue) {
                            setState(() {
                              _gender = newValue!;
                            });
                          },
                          items: <String>[
                            'male',
                            'female',
                            'other',
                          ].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                        DropdownButton<String>(
                          value: _bloodGroup,
                          onChanged: (String? newValue) {
                            setState(() {
                              _bloodGroup = newValue!;
                            });
                          },
                          items: <String>['A+', 'A-', 'AB+', 'AB-', 'B+', 'B-', 'O+', 'O-'].map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value),
                            );
                          }).toList(),
                        ),
                        TextFormField(
                          decoration: const InputDecoration(
                            border: OutlineInputBorder(),
                            labelText: 'Phone Number',
                          ),
                          onSaved: (input) {
                            _phoneNum = input!;
                          },
                          validator: (input) => (input!.isEmpty) ? 'Phone Number is Required' : null,
                        ),
                      ],
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: ElevatedButton(
                    onPressed: _validateForm,
                    child: const Text('Sign In with Google'),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
