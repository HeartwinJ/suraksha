import 'package:flutter/material.dart';
import 'package:suraksha/common/dashboard.dart';
import 'package:suraksha/services/auth_service.dart';
import 'package:suraksha/services/proximity_service.dart';

class UserDashboard extends StatelessWidget {
  const UserDashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('User Dashboard'),
        actions: [
          IconButton(
            onPressed: () {
              AuthService _auth = AuthService();
              _auth.signOut();
            },
            icon: const Icon(Icons.logout),
          ),
        ],
      ),
      body: const DashboardComponent(),
    );
  }
}
