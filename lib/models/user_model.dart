class UserModel {
  final String? uid;
  final String name;
  final String email;
  final String designation;
  final String department;
  final String gender;
  final String bloodGroup;
  final String phoneNum;
  final String profileStatus;
  final double? latitude;
  final double? longitude;

  UserModel({
    this.uid,
    required this.name,
    required this.email,
    required this.designation,
    required this.department,
    required this.gender,
    required this.bloodGroup,
    required this.phoneNum,
    required this.profileStatus,
    this.latitude,
    this.longitude,
  });
}
